package com.gitlab.atomfrede.codecov.demo;

import org.junit.AfterClass;
import org.junit.BeforeClass;

public class SparkTest {

    @BeforeClass
    public static void ignite() {

        Server.igniteSpark(true);
    }

    @AfterClass
    public static void extinguish() throws InterruptedException {

        Server.extinguishSpark();

        Thread.sleep(250);
    }
}
